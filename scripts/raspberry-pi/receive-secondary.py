#!/usr/bin/python3

import time, datetime
import shutil
import busio
import board
from digitalio import DigitalInOut
import adafruit_rfm69
import re
from statistics import median
import sys

i2c = busio.I2C(board.SCL, board.SDA)
CS = DigitalInOut(board.CE1)
RESET = DigitalInOut(board.D25)
spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
rfm69 = adafruit_rfm69.RFM69(spi, CS, RESET, 915.0)
rfm69.encryption_key = b'{\xc0\xee.5!d$!\xe2L@,$\r\xdb'
packet = None
timeoutCount = time.time()

while packet is None:
    packet = rfm69.receive()
    time.sleep(0.25) # seconds

    # 30 seconds for timeout
    if (time.time() - timeoutCount >= 30):
        print("No data received in 30 seconds. Exiting.")
        sys.exit()
try:
    decoded = packet.decode()
except:
    print("Error decoding radio data. Exiting")
    sys.exit()

filtered = re.findall('R([0-9]{4})', decoded)

#print(filtered)

measurementList = []

for f in filtered:
   measurementList.append(int(f))

# Uncomment to set distance value as the **median** of all readings, in mm
#rawDistanceValue = median(measurementList)

# Uncomment to set the distance value as the **mode** of all readings, in mm
rawDistanceValue = max(set(measurementList), key=measurementList.count)

print(rawDistanceValue)

correctedDistanceValue = rawDistanceValue - 490 # Distance between max fill level and actual fill level, accounting for sensor placement, 1.6ft
waterLevel = 2286 - correctedDistanceValue # Max fill level is 7'5"
waterLevel = (waterLevel / 304.8) # Convert mm's to feet
waterLevel = round(waterLevel, 1)

print(waterLevel, file=open("/home/pi/TANK-DATA/.CURRENT_VALUE", "w"))
print(str(datetime.datetime.now())[:16], file=open("/home/pi/TANK-DATA/.CURRENT_VALUE", "a"))

shutil.move("/home/pi/TANK-DATA/.CURRENT_VALUE", "/home/pi/TANK-DATA/CURRENT_VALUE")

