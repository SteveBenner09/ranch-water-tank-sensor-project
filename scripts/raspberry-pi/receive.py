#!/usr/bin/python3

import time, datetime
import shutil
import busio
import board
from digitalio import DigitalInOut
import adafruit_rfm69
import re

i2c = busio.I2C(board.SCL, board.SDA)
CS = DigitalInOut(board.CE1)
RESET = DigitalInOut(board.D25)
spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
rfm69 = adafruit_rfm69.RFM69(spi, CS, RESET, 915.0)
rfm69.encryption_key = b'{\xc0\xee.5!d$!\xe2L@,$\r\xdb'

while packet is None:
    packet = rfm69.receive()
    time.sleep(0.5)
    decoded = packet.decode()

filter = re.findall('R([0-9]{4})', decoded)
print(filter)


"""

distanceValue = int(packetText) # Distance from sensor to water in mm
correctedDistanceValue = distanceValue - 490 # Distance between sensor and the max water level
waterLevel = 2286 - correctedDistanceValue
waterLevel = (waterLevel / 304.8) # Convert mm's to feet
waterLevel = round(waterLevel, 1)

#date = datetime.datetime.strptime(d1, "%a %b %d %H:%M:%S %Y")
#print(date)

print(waterLevel, file=open("/home/pi/TANK-DATA/.CURRENT_VALUE", "w"))
print(str(datetime.datetime.now())[:16], file=open("/home/pi/TANK-DATA/.CURRENT_VALUE", "a"))

shutil.move("/home/pi/TANK-DATA/.CURRENT_VALUE", "/home/pi/TANK-DATA/CURRENT_VALUE")

"""
