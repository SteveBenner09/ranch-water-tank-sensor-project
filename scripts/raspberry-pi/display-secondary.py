#/usr/bin/python3

import busio
import board
from RPLCD.i2c import CharLCD
from digitalio import DigitalInOut

# LCD Setup
lcd = CharLCD(i2c_expander='PCF8574', address=0x27, port=1,
              cols=20, rows=4, dotsize=8,
              charmap='A02',
              backlight_enabled=True)

def printLines(line1, line2, line3, line4):

	lcd.write_string(line1)
	lcd.cursor_pos = (1, 0)
	lcd.write_string(line2)
	lcd.cursor_pos = (2, 0)
	lcd.write_string(line3)
	lcd.cursor_pos = (3, 0)
	lcd.write_string(line4)

	return None

# Get values written to file by automated tank data receiving script
output = open("/home/pi/TANK-DATA/CURRENT_VALUE", "r")
reading = output.readline().rstrip("\n")
datetime = output.readline().rstrip("\n")

time = datetime[11:16]
date = datetime[:10]

line1="{0}"
line2=""
line3="As Of: {0}"
line4="Tank Level: {0} feet"

if (time.time() - time >= 3600):
	

# Write values to display
printLines(line1.format(date), line2, line3.format(time), line4.format(reading))
